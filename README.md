# Installation
Replace `engines.json` in:

+ **Linux** : `~/Zotero/locate/engines.json`  
+ **Mac** : `/Users/<username>/Zotero/locate/engines.json`  
+ **Windows** : `C:\Users\<username>\Zotero/locate/engines.json`

# Locate

**Before**

![default](https://www.zotero.org/support/_media/locate/locate-menu.png)

**After**

![screenshot](screenshot.png)

# Engines

|     | Name | Description |
|:---:| ---  |     ---     |
| ![base](https://www.base-search.net/favicon.ico) | [BASE](https://www.base-search.net) | Open Access search engine |
| ![unicat](https://www.unicat.be/uniCatImages/favicon.ico) | [Unicat](https://www.unicat.be) |  Union Catalogue of Belgian Libraries  |
| ![cible+](http://cibleplus.ulb.ac.be/favicon.ico) | [Cible+](http://cibleplus.ulb.ac.be) | Moteur de recherche des bibliothèques de l'ULB (filtre : bibliothèque d'architecture) |
| ![libgen](https://libgen.fun/favicon.ico) | [LibGen](https://libgen.fun) | File-sharing website for print publications |
| ![zlib](https://booksc.org/favicon-16x16.svg?v=2)| [Z-Library](https://booksc.org) | File-sharing site for journal articles, books, and magazines |
| x | [Sci-Hub](https://sci-hub.se) | Scientific research paper file sharing website |
| ![archires](https://www.archires.archi.fr/sites/all/themes/ensa_theme/favicon.ico) | [ArchiRès](https://www.archires.archi.fr) | Portail francophone des bibliothèques d'écoles d'architecture et de paysage |
| ![theses](https://www.theses.fr/img/favicon.png) | [thèses.fr](https://www.theses.fr) | Moteur de recherche des thèses de doctorat françaises |
| ![erara](https://www.e-rara.ch/erara/domainimage/favicons/favicon-16x16.png) | [e-rara](https://www.e-rara.ch) | The platform for digitized rare books from Swiss institutions |
| ![gallica](https://gallica.bnf.fr/accueil/sites/default/files/index.ico) | [Gallica](https://gallica.bnf.fr) | Bibliothèque numérique de la BnF et de ses partenaires |
| ![openedition](https://search.openedition.org/favicon.ico) | [OpenEdition](https://search.openedition.org) | Portail de ressources électronique en sciences humaines et sociales |
| ![paperity](https://paperity.org/static/img/favicon.ico) | [Paperity](https://paperity.org) | A multidisciplinary aggregator of Open Access journals and papers |
| ![unpaywall](https://unpaywall.org/favicon.png) | [Unpaywall](https://oadoi.org) |  An open database of 20 million free scholarly articles |
| ![acatorrent](https://academictorrents.com/favicon.ico) | [Academic Torrents](https://academictorrents.com) | A distributed system for sharing enormous datasets |
| ![crossref](https://crossref.org/favicon.ico) | [CrossRef](https://crossref.org) 		| Digital object identifier (DOI) Registration Agency |
| ![semantic](https://cdn.semanticscholar.org/0a753e80ed472b70/img/favicon-16x16.png) | [Semantic Scholar](https://www.semanticscholar.org) | A free, AI-powered research tool for scientific literature |
